package com.pulque.androidxmpp.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pulque.androidxmpp.R;
import com.pulque.androidxmpp.entity.FriendRooms;

import java.util.ArrayList;

/**
 * Created by Pulque.Li on 3/9/2015.
 */
public class RoomListAdapter extends BaseAdapter {

    private final class ViewHolder {
        ImageView icon_presence;
        TextView Occupants;
        TextView message;
        TextView name;
    }

    private Context mContext;
    private ArrayList<FriendRooms> mArrayList;

    public RoomListAdapter(Context ctx, ArrayList<FriendRooms> mArrayList) {
        mContext = ctx;
        this.mArrayList = mArrayList;
    }

    @Override
    public int getCount() {
        return mArrayList != null ? mArrayList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.room_list_item, parent, false);

            holder.icon_presence = (ImageView) convertView
                    .findViewById(R.id.icon_presence);
            holder.name = (TextView) convertView
                    .findViewById(R.id.name);
            holder.message = (TextView) convertView
                    .findViewById(R.id.message);
            holder.Occupants = (TextView) convertView
                    .findViewById(R.id.Occupants);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        FriendRooms mEntity = mArrayList.get(position);
        holder.name.setText(mEntity.getName());
        holder.message.setText(mEntity.getDescription());
        holder.Occupants.setText(mEntity.getOccupants() + "");
        holder.icon_presence.setImageResource(R.drawable.chat);
        return convertView;
    }
}
