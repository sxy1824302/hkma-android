package com.pulque.androidxmpp.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.pulque.androidxmpp.R;
import com.pulque.androidxmpp.entity.UserInfo;
import com.pulque.androidxmpp.service.XmppConnectionService;
import com.pulque.androidxmpp.tools.DEF;
import com.pulque.androidxmpp.tools.QiniuUploadUitls;
import com.pulque.androidxmpp.tools.Utils;
import com.qiniu.android.common.Config;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;

import junit.framework.Assert;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends Activity {

    TextView textFetchPassWord = null, textRegister = null;
    Button loginButton = null;
    ImageButton listIndicatorButton = null, deleteButtonOfEdit = null;
    ImageView currentUserImage = null;
    ListView loginList = null;
    EditText qqEdit = null, passwordEdit = null;
    private static boolean isVisible = false;         //ListView isVisible
    private static boolean isIndicatorUp = false;

    public static int currentSelectedPosition = -1;

    String[] from = {"userPhoto", "userQQ", "pwd", "deletButton"};
    int[] to = {R.id.login_userPhoto, R.id.login_userQQ, 0, R.id.login_deleteButton};
    ArrayList<HashMap<String, Object>> list = null;
    private MsgReceiver msgReceiver;
    private NiftyDialogBuilder dialogBuilder;


//
//    public void testFile(String token) throws Throwable {
//
////        String token = "从服务端SDK获取";
//        UploadManager uploadManager = new UploadManager();
//        uploadManager.put("/sdcard/contact_0.jpg", "contact_0.jpg", token,
//                new UpCompletionHandler() {
//                    @Override
//                    public void complete(String key, ResponseInfo info, JSONObject response) {
//                        Log.e("qiniu", info.toString());
//                    }
//                }, null);
//
//
//
//    }
//
//
    /**
     * 上传从相册选择的图片
     * @param result
     */
    private void uploadBitmap(String result) {
        QiniuUploadUitls.getInstance().uploadImage(result, new QiniuUploadUitls.QiniuUploadUitlsListener() {

            @Override
            public void onSucess(String fileUrl) {
                // TODO Auto-generated method stub
               // showUrlImage(fileUrl);

                Log.e("onSucess","fileUrl==="+fileUrl);
            }

            @Override
            public void onProgress(int progress) {
                // TODO Auto-generated method stub
                //pbProgress.setProgress(progress);
                Log.e("onProgress","onProgress==="+progress);
            }

            @Override
            public void onError(int errorCode, String msg) {
                // TODO Auto-generated method stub
                //showToast("errorCode=" + errorCode + ",msg=" + msg);
                Log.e("onError","onError==="+msg);
            }
        });
    }

    public void testFile(String token) throws Throwable {

//        String token = "从服务端SDK获取";
        UploadManager uploadManager = new UploadManager();
        uploadManager.put("/sdcard/contact_0.jpg", "contact_0.jpg", token,
                new UpCompletionHandler() {
                    @Override
                    public void complete(String key, ResponseInfo info, JSONObject response) {
                        Log.e("qiniu", info.toString());
                    }
                }, null);



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);



//new Thread(new Runnable() {
//    @Override
//    public void run() {
//        String json = Utils.executeLoginPost();
//        try {
//            JSONObject mJsonObject = new JSONObject(json);
//
//            String token = mJsonObject.optString("access_token");
//Log.e("token","token=="+token);
//            testFile(token);
//
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
//        uploadBitmap("/sdcard/contact_0.jpg");
//    }
//}).start();

//new Thread(new Runnable() {
//    @Override
//    public void run() {
//        String json = Utils.executeLoginPost();
//        try {
//            JSONObject mJsonObject = new JSONObject(json);
//
//            String token = mJsonObject.optString("access_token");
//
//            testFile(token);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
//
//    }
//}).start();

























        msgReceiver = new MsgReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DEF.ACTION_RECEIVER);
        intentFilter.addAction(DEF.ACTION_LOGIN);
        intentFilter.addAction(DEF.ACTION_ISCONNECTED_FAILED);
        registerReceiver(msgReceiver, intentFilter);


        setContentView(R.layout.activity_login);

        textFetchPassWord = (TextView) findViewById(R.id.fetchPassword);
        textFetchPassWord.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                View row = View.inflate(LoginActivity.this, R.layout.setting_dialog, null);
                final EditText domain = (EditText) row.findViewById(R.id.domain);
                final EditText resource = (EditText) row.findViewById(R.id.resource);

                final NiftyDialogBuilder LogindialogBuilder = NiftyDialogBuilder.getInstance(LoginActivity.this);
                LogindialogBuilder
                        .withTitle("Setting")                                  //.withTitle(null)  no title
                        .withTitleColor("#000000")                                  //def
                        .withDividerColor("#11ffffff")                           //def  | withMessageColor(int resid)
                        .withDialogColor("#FFdde1ee")                               //def  | withDialogColor(int resid)
                        .withIcon(getResources().getDrawable(android.R.drawable.ic_menu_info_details))
                        .withDuration(700)                                          //def
                        .withEffect(Effectstype.Fliph)                                         //def Effectstype.Slidetop
                        .withButton1Text("OK")                                      //def gone
                        .withButton2Text("Cancel")                                  //def gone
                        .isCancelableOnTouchOutside(true)                           //def    | isCancelable(true)
                        .setCustomView(row, v.getContext())         //.setCustomView(View or ResId,context)
                        .setButton1Click(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String mDomain = domain.getText().toString();
                                if (!TextUtils.isEmpty(mDomain)) {
                                    DEF.DOMAIN = mDomain;
                                } else {
                                    Utils.showToast(LoginActivity.this, "Domain is null");
                                    return;
                                }

                                String mResource = resource.getText().toString();
                                if (!TextUtils.isEmpty(mResource)) {
                                    DEF.RESOURCE = mResource;
                                } else {
                                    Utils.showToast(LoginActivity.this, "Resource is null");
                                    return;
                                }
                                Utils.showToast(LoginActivity.this, "Setting Success!");
                                LogindialogBuilder.dismiss();
                            }
                        })
                        .setButton2Click(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                LogindialogBuilder.dismiss();
                            }
                        })
                        .show();
            }
        });
        textRegister = (TextView) findViewById(R.id.registQQ);
        textRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                View row = View.inflate(LoginActivity.this, R.layout.setting_dialog, null);
                final TextView title1 = (TextView) row.findViewById(R.id.title1);
                title1.setText("ID");
                final TextView title2 = (TextView) row.findViewById(R.id.title2);
                title2.setText("PWD");
                final EditText domain = (EditText) row.findViewById(R.id.domain);
                domain.setText("");
                final EditText resource = (EditText) row.findViewById(R.id.resource);
                resource.setText("");

                dialogBuilder = NiftyDialogBuilder.getInstance(LoginActivity.this);
                dialogBuilder
                        .withTitle("Setting")                                  //.withTitle(null)  no title
                        .withTitleColor("#000000")                                  //def
                        .withDividerColor("#11ffffff")                           //def  | withMessageColor(int resid)
                        .withDialogColor("#FFdde1ee")                               //def  | withDialogColor(int resid)
                        .withIcon(getResources().getDrawable(android.R.drawable.ic_menu_info_details))
                        .withDuration(700)                                          //def
                        .withEffect(Effectstype.Fliph)                                         //def Effectstype.Slidetop
                        .withButton1Text("Register")                                      //def gone
                        .withButton2Text("Cancel")                                  //def gone
                        .isCancelableOnTouchOutside(true)                           //def    | isCancelable(true)
                        .setCustomView(row, v.getContext())         //.setCustomView(View or ResId,context)
                        .setButton1Click(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String mDomain = domain.getText().toString();
                                if (TextUtils.isEmpty(mDomain)) {
                                    Utils.showToast(LoginActivity.this, "ID is null");
                                    return;
                                }

                                String mResource = resource.getText().toString();
                                if (TextUtils.isEmpty(mResource)) {
                                    Utils.showToast(LoginActivity.this, "PWD is null");
                                    return;
                                }
                                final Intent intentService = new Intent(LoginActivity.this, XmppConnectionService.class);
                                intentService.putExtra(XmppConnectionService.TAG_SERVICE, XmppConnectionService.TAG_REGISTER);
                                intentService.putExtra(XmppConnectionService.LOGIN, mDomain);
                                intentService.putExtra(XmppConnectionService.PASSWORD, mResource);
                                intentService.putExtra(XmppConnectionService.SERVER_HOST, DEF.DOMAIN);
                                startService(intentService);
                                Utils.showPD(LoginActivity.this, DEF.SHOW_PD_SUBMITTING);
                            }
                        })
                        .setButton2Click(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogBuilder.dismiss();
                            }
                        })
                        .show();
            }
        });
        loginButton = (Button) findViewById(R.id.qqLoginButton);
        listIndicatorButton = (ImageButton) findViewById(R.id.qqListIndicator);
        loginList = (ListView) findViewById(R.id.loginQQList);
        list = new ArrayList<HashMap<String, Object>>();
        currentUserImage = (ImageView) findViewById(R.id.myImage);
        qqEdit = (EditText) findViewById(R.id.qqNum);
        passwordEdit = (EditText) findViewById(R.id.qqPassword);
        deleteButtonOfEdit = (ImageButton) findViewById(R.id.delete_button_edit);


        qqEdit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (qqEdit.getText().toString().equals("") == false) {
                    deleteButtonOfEdit.setVisibility(View.VISIBLE);
                }

            }
        });

        deleteButtonOfEdit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                currentUserImage.setImageResource(R.drawable.contact_0);
                qqEdit.setText("");
                currentSelectedPosition = -1;
                deleteButtonOfEdit.setVisibility(View.GONE);

            }
        });

        UserInfo user1 = new UserInfo(R.drawable.contact_0, "androidtester", "123123", R.drawable.deletebutton);
        UserInfo user2 = new UserInfo(R.drawable.contact_1, "pulque", "123123", R.drawable.deletebutton);
        addUser(user1);
        addUser(user2);

        if (currentSelectedPosition == -1) {
            currentUserImage.setImageResource(R.drawable.contact_0);
            qqEdit.setText("");
        } else {
            currentUserImage.setImageResource((Integer) list.get(currentSelectedPosition).get(from[0]));
            qqEdit.setText((String) list.get(currentSelectedPosition).get(from[1]));
        }

        MyLoginListAdapter adapter = new MyLoginListAdapter(this, list, R.layout.layout_list_item, from, to);
        loginList.setAdapter(adapter);
        loginList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                currentUserImage.setImageResource((Integer) list.get(arg2).get(from[0]));
                qqEdit.setText((String) list.get(arg2).get(from[1]));
                passwordEdit.setText((String) list.get(arg2).get(from[2]));
                currentSelectedPosition = arg2;

                loginList.setVisibility(View.GONE);
                listIndicatorButton.setBackgroundResource(R.drawable.indicator_down);

            }


        });

        listIndicatorButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (isIndicatorUp) {
                    isIndicatorUp = false;
                    isVisible = false;
                    listIndicatorButton.setBackgroundResource(R.drawable.indicator_down);
                    loginList.setVisibility(View.GONE);

                } else {
                    isIndicatorUp = true;
                    isVisible = true;
                    listIndicatorButton.setBackgroundResource(R.drawable.indicator_up);
                    loginList.setVisibility(View.VISIBLE);
                }
            }

        });

        loginButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String serverHost = DEF.DOMAIN;
                String login = qqEdit.getText().toString();
                String resource = DEF.RESOURCE;
                String password = passwordEdit.getText().toString();
                if (TextUtils.isEmpty(login)) {
                    Utils.showToast(LoginActivity.this, "ID is null!");
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Utils.showToast(LoginActivity.this, "Password is null!");
                    return;
                }

                final Intent intentService = new Intent(LoginActivity.this, XmppConnectionService.class);
                intentService.putExtra(XmppConnectionService.TAG_SERVICE, XmppConnectionService.TAG_LOGIN);
                intentService.putExtra(XmppConnectionService.SERVER_HOST, serverHost);
                intentService.putExtra(XmppConnectionService.LOGIN, login);
                intentService.putExtra(XmppConnectionService.PASSWORD, password);
                intentService.putExtra(XmppConnectionService.RESOURCE, resource);
                startService(intentService);

                Utils.showPD(LoginActivity.this, DEF.SHOW_PD_LOGIN);
            }

        });

    }


    private void addUser(UserInfo user) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(from[0], user.userPhoto);
        map.put(from[1], user.userQQ);
        map.put(from[2], user.userPassword);
        map.put(from[3], user.deleteButtonRes);

        list.add(map);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN && isVisible) {
            int[] location = new int[2];

            loginList.getLocationInWindow(location);
            int x = (int) event.getX();
            int y = (int) event.getY();
            if (x < location[0] || x > location[0] + loginList.getWidth() ||
                    y < location[1] || y > location[1] + loginList.getHeight()) {
                isIndicatorUp = false;
                isVisible = false;


                listIndicatorButton.setBackgroundResource(R.drawable.indicator_down);
                loginList.setVisibility(View.GONE);

            }


        }

        return super.onTouchEvent(event);
    }

    public class MyLoginListAdapter extends BaseAdapter {

        protected Context context;
        protected ArrayList<HashMap<String, Object>> list;
        protected int itemLayout;
        protected String[] from;
        protected int[] to;


        public MyLoginListAdapter(Context context,
                                  ArrayList<HashMap<String, Object>> list, int itemLayout,
                                  String[] from, int[] to) {
            super();
            this.context = context;
            this.list = list;
            this.itemLayout = itemLayout;
            this.from = from;
            this.to = to;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        class ViewHolder {
            public ImageView userPhoto;
            public TextView userQQ;
            public ImageButton deleteButton;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            ViewHolder holder = null;

            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(itemLayout, null);
                holder = new ViewHolder();
                holder.userPhoto = (ImageView) convertView.findViewById(to[0]);
                holder.userQQ = (TextView) convertView.findViewById(to[1]);
                holder.deleteButton = (ImageButton) convertView.findViewById(to[3]);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.userPhoto.setBackgroundResource((Integer) list.get(position).get(from[0]));
            holder.userQQ.setText((String) list.get(position).get(from[1]));
            holder.deleteButton.setBackgroundResource((Integer) list.get(position).get(from[3]));
            holder.deleteButton.setOnClickListener(new ListOnClickListener(position));

            return convertView;
        }

        class ListOnClickListener implements OnClickListener {

            private int position;


            public ListOnClickListener(int position) {
                super();
                this.position = position;
            }

            @Override
            public void onClick(View arg0) {
                list.remove(position);

                if (position == currentSelectedPosition) {
                    currentUserImage.setImageResource(R.drawable.contact_0);
                    qqEdit.setText("");
                    currentSelectedPosition = -1;
                } else if (position < currentSelectedPosition) {
                    currentSelectedPosition--;
                }

                listIndicatorButton.setBackgroundResource(R.drawable.indicator_down);
                loginList.setVisibility(View.GONE);

                MyLoginListAdapter.this.notifyDataSetChanged();


            }

        }


    }


    public class MsgReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(DEF.ACTION_RECEIVER)) {
                String mString = intent.getStringExtra(XmppConnectionService.RESULT);
                Log.e("ActivityRegister", "onResume===" + mString);
                Utils.dismissPD();
                if (!TextUtils.isEmpty(mString)) {
                    if (mString.equals(DEF.NOT_RESULT)) {
                        Utils.showToast(getApplicationContext(), DEF.NOT_RESULT);
                    } else if (mString.equals(DEF.ALREADY_EXISTS)) {
                        Utils.showToast(getApplicationContext(), DEF.ALREADY_EXISTS);
                    } else if (mString.equals(DEF.REGISTRATION_FAILED)) {
                        Utils.showToast(getApplicationContext(), DEF.REGISTRATION_FAILED);
                    } else if (mString.equals(DEF.SUCCESSFUL_REGISTRATION)) {
                        Utils.showToast(getApplicationContext(), DEF.SUCCESSFUL_REGISTRATION);
                        dialogBuilder.dismiss();
                    }
                }
            } else if (intent.getAction().equals(DEF.ACTION_LOGIN)) {
                Utils.dismissPD();
                Utils.showToast(getApplicationContext(), "ID or Password is not match, Login failed!");
            } else if (intent.getAction().equals(DEF.ACTION_ISCONNECTED_FAILED)) {
                Utils.dismissPD();
                Utils.showToast(getApplicationContext(), "Failed to connect to " + DEF.DOMAIN);
            }

        }

    }

}
