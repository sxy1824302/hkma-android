package com.pulque.androidxmpp.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.pulque.androidxmpp.R;
import com.pulque.androidxmpp.adapter.RoomListAdapter;
import com.pulque.androidxmpp.entity.FriendRooms;
import com.pulque.androidxmpp.service.IXmppConnectionService;
import com.pulque.androidxmpp.service.XMPPRequest;
import com.pulque.androidxmpp.service.XmppConnectionService;
import com.pulque.androidxmpp.service.XmppConnectionServiceBinder;
import com.pulque.androidxmpp.tools.DEF;
import com.pulque.androidxmpp.tools.Utils;
import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;
import com.yalantis.contextmenu.lib.MenuObject;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemClickListener;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.muc.MultiUserChat;

import java.util.ArrayList;
import java.util.List;

public class RoomListActivity extends ActionBarActivity implements ServiceConnection, OnMenuItemClickListener {

    private static final String TAG = "RoomListActivity";

    private ArrayList<FriendRooms> RoomList;
    private RoomListAdapter ListAdapter;
    private SwipeMenuListView listView;
    private ContextMenuDialogFragment mMenuDialogFragment;
    private IXmppConnectionService xmppService;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent connectionService = new Intent(this, XmppConnectionService.class);
        bindService(connectionService, this, Context.BIND_AUTO_CREATE);

        setContentView(R.layout.activity_room_list);

        MsgReceiver msgReceiver = new MsgReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DEF.ACTION_INVITE);
        registerReceiver(msgReceiver, intentFilter);

        listView = (SwipeMenuListView) findViewById(R.id.listView);

        RoomList = new ArrayList<FriendRooms>();
        ListAdapter = new RoomListAdapter(getApplicationContext(), RoomList);
        listView.setAdapter(ListAdapter);
        listView.setTextFilterEnabled(true);

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                        0xCE)));
                // set item width
                openItem.setWidth(Utils.dp2px(RoomListActivity.this, 90));
                // set item title
                openItem.setTitle("Join");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(Utils.dp2px(RoomListActivity.this, 90));
                // set a icon
                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        listView.setMenuCreator(creator);

        // step 2. listener item click event
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                FriendRooms item = RoomList.get(position);
                switch (index) {
                    case 0:
                        // open
                        MultiUserChat muc = XMPPRequest.joinMultiUserChat(xmppService.getConnection().getUser(), "", item.getName(), xmppService.getConnection());
                        if (muc == null) {
                            Utils.showToast(RoomListActivity.this, "Join failed");
                        } else {
                            Utils.showToast(RoomListActivity.this, "Join success");
                            Intent intent = new Intent(RoomListActivity.this, RoomActivity.class);
                            intent.putExtra("user", item.getName());
                            startActivity(intent);
                        }
                        break;
                    case 1:
                        // delete
                        Utils.showToast(RoomListActivity.this, "How to delete room?");
//                        mAppList.remove(position);
//                        mAdapter.notifyDataSetChanged();
                        break;
                }
                return false;
            }
        });

        initToolbar();

        ArrayList<MenuObject> menuObjects = new ArrayList<>();
        menuObjects.add(new MenuObject(R.drawable.icn_close));
        menuObjects.add(new MenuObject(R.drawable.icn_black, "Black List"));
        menuObjects.add(new MenuObject(R.drawable.icn_refresh, "Refresh Rooms"));
        menuObjects.add(new MenuObject(R.drawable.icn_create, "Create Room"));

        mMenuDialogFragment = ContextMenuDialogFragment.newInstance((int) getResources().getDimension(R.dimen.tool_bar_height), menuObjects);

    }

    private void initToolbar() {
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    private FriendRooms searchAccount(String user) {

        for (FriendRooms account : RoomList) {
            if (account.getName().equals(user)) {
                return account;
            }
        }

        return null;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.i(TAG, "Connected!");
        if (service == null)
            return;
        xmppService = ((XmppConnectionServiceBinder) service).getService();
        if (xmppService == null)
            return;
        String myname = xmppService.getConnection().getUser();
        if (!TextUtils.isEmpty(myname) && myname.indexOf("@") != -1) {
            getSupportActionBar().setTitle(myname.substring(0, myname.indexOf("@")));
        }
        refreshRoom();

    }

    private void refreshRoom() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (xmppService == null)
                    return;
                try {
                    RoomList.clear();
                    RoomList.addAll(XMPPRequest.getConferenceRoom(xmppService.getConnection()));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ListAdapter.notifyDataSetChanged();
                        }
                    });

                } catch (XMPPException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.i(TAG, "Disconnected!");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.context_menu:
                mMenuDialogFragment.show(getSupportFragmentManager(), "DropDownMenuFragment");
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMenuItemClick(View clickedView, int position) {
        switch (position) {
            case 0:
                break;
            case 1:
                Utils.showToast(RoomListActivity.this, "Black List?");
                break;
            case 2:
                refreshRoom();
                break;
            case 3:
                final EditText et = new EditText(this);
                et.setPadding(10, 10, 10, 10);

                final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(RoomListActivity.this);
                dialogBuilder
                        .withTitle(getString(R.string.Room_Name))                                  //.withTitle(null)  no title
                        .withTitleColor("#000000")                                  //def
                        .withDividerColor("#11ffffff")                           //def  | withMessageColor(int resid)
                        .withDialogColor("#FFdde1ee")                               //def  | withDialogColor(int resid)
                        .withIcon(getResources().getDrawable(android.R.drawable.ic_menu_info_details))
                        .withDuration(700)                                          //def
                        .withEffect(Effectstype.Fliph)                                         //def Effectstype.Slidetop
                        .withButton1Text("Create")                                      //def gone
                        .withButton2Text("Cancel")                                  //def gone
                        .isCancelableOnTouchOutside(true)                           //def    | isCancelable(true)
                        .setCustomView(et, RoomListActivity.this)         //.setCustomView(View or ResId,context)
                        .setButton1Click(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        XMPPRequest.createRoom(xmppService.getConnection(), et.getText().toString());
                                        refreshRoom();
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                dialogBuilder.dismiss();
                                            }
                                        });
                                    }
                                }).start();

                            }
                        })
                        .setButton2Click(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogBuilder.dismiss();
                            }
                        })
                        .show();
                break;


        }
    }

    public class MsgReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(DEF.ACTION_INVITE)) {
                final TextView et = new TextView(RoomListActivity.this);
                et.setPadding(10, 10, 10, 10);
                et.setText(intent.getStringExtra(DEF.ACTION_INVITE));

                final NiftyDialogBuilder dialogBuilder1 = NiftyDialogBuilder.getInstance(RoomListActivity.this);
                dialogBuilder1
                        .withTitle("Invite")                                  //.withTitle(null)  no title
                        .withTitleColor("#000000")                                  //def
                        .withDividerColor("#11ffffff")                           //def  | withMessageColor(int resid)
                        .withDialogColor("#FFdde1ee")                               //def  | withDialogColor(int resid)
                        .withIcon(getResources().getDrawable(android.R.drawable.ic_menu_info_details))
                        .withDuration(700)                                          //def
                        .withEffect(Effectstype.Fliph)                                         //def Effectstype.Slidetop
                        .withButton1Text("OK")                                   //def gone
                        .isCancelableOnTouchOutside(true)                           //def    | isCancelable(true)
                        .setCustomView(et, RoomListActivity.this)         //.setCustomView(View or ResId,context)
                        .setButton1Click(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                dialogBuilder1.dismiss();
                                            }
                                        });

                                    }
                                }).start();

                            }
                        })
                        .show();
            }

        }

    }
}
