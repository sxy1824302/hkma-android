package com.pulque.androidxmpp.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.jialin.chat.Message;
import com.jialin.chat.MessageAdapter;
import com.jialin.chat.MessageInputToolBox;
import com.jialin.chat.OnOperationListener;
import com.jialin.chat.Option;
import com.jialin.chat.RecordButton;
import com.pulque.androidxmpp.R;
import com.pulque.androidxmpp.service.IXmppConnectionService;
import com.pulque.androidxmpp.service.XMPPRequest;
import com.pulque.androidxmpp.service.XmppConnectionService;
import com.pulque.androidxmpp.service.XmppConnectionServiceBinder;
import com.pulque.androidxmpp.tools.DEF;
import com.pulque.androidxmpp.tools.MessageListener;
import com.pulque.androidxmpp.tools.ParticipantStatus;
import com.pulque.androidxmpp.tools.QiniuUploadUitls;
import com.pulque.androidxmpp.tools.Utils;
import com.squareup.picasso.Picasso;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.MultiUserChat;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RoomActivity extends FragmentActivity implements ServiceConnection {

    private static final String TAG = "RoomActivity";
    private final int REQUEST_CODE_CAPTURE_CAMEIA = 0;
    private final int REQUEST_CODE_PICK_IMAGE = 1;
    private String selectedImagePath;
    public static final String TEMP_JPG_PATH = "/data/data/com.pulque.androidxmpp/files/temp.jpg";

    private MessageInputToolBox box;
    private ListView listView;
    private MessageAdapter adapter;
    private IXmppConnectionService xmppService;
    private ImageView showImage;

    @SuppressLint("UseSparseArrays")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent connectionService = new Intent(this, XmppConnectionService.class);
        bindService(connectionService, this, Context.BIND_AUTO_CREATE);
        setContentView(R.layout.activity_room);

        MsgReceiver msgReceiver = new MsgReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DEF.ACTION_INVITE);
        registerReceiver(msgReceiver, intentFilter);

        initMessageInputToolBox();

        initListView();
    }

    /**
     * init MessageInputToolBox
     */
    @SuppressLint("ShowToast")
    private void initMessageInputToolBox() {
        box = (MessageInputToolBox) findViewById(R.id.messageInputToolBox);
        box.setOnFinishedRecordListener(new RecordButton.OnFinishedRecordListener() {
            @Override
            public void onFinishedRecord(String audioPath) {
                uploadRecord(audioPath);
            }
        });
        box.setOnOperationListener(new OnOperationListener() {

            @Override
            public void send(String content) {

                System.out.println("===============" + content);
//                Message message = new Message(0, 1, "Tom", "avatar", "Jerry", "avatar", content, true, true, new Date());
                Message message = XMPPRequest.sendMessage(content);
//                if (message != null)
//                    adapter.getData().add(message);
//                listView.setSelection(listView.getBottom());
//
//                //Just demo
//                createReplayMsg(message);
            }

            @Override
            public void selectedFace(String content) {

                System.out.println("===============" + content);
//                Message message = new Message(Message.MSG_TYPE_TEXT, Message.MSG_STATE_SUCCESS, "Tomcat", "avatar", "Jerry", "avatar", content, true, true, new Date());
//                adapter.getData().add(message);


                Message message = XMPPRequest.sendMessage(content);
//                if (message != null)
//                    adapter.getData().add(message);
//                listView.setSelection(listView.getBottom());

//                //Just demo
//                createReplayMsg(message);
            }


            @Override
            public void selectedFuncation(int index) {

                System.out.println("===============" + index);

                switch (index) {
                    case 0:
                        getImageFromAlbum();
                        //do some thing
//                        mDialog = new AlertDialog.Builder(mContext).setMultiChoiceItems(
//                                new String[] { getString(R.string.take_a_photo),
//                                        getString(R.string.select_from_galary) }, null,
//                                new OnMultiChoiceClickListener() {
//
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which,
//                                                        boolean isChecked) {
//                                        if (which == 0) {
//                                            getImageFromCamera();
//                                        } else {
//                                            getImageFromAlbum();
//                                        }
//                                        mDialog.dismiss();
//                                    }
//                                }).show();
                        break;
                    case 1:
                        //do some thing
                        getImageFromCamera();
                        break;
                    case 2:
                        final EditText et = new EditText(RoomActivity.this);
                        et.setPadding(10, 10, 10, 10);

                        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(RoomActivity.this);
                        dialogBuilder
                                .withTitle("Invite")                                  //.withTitle(null)  no title
                                .withTitleColor("#000000")                                  //def
                                .withDividerColor("#11ffffff")                           //def  | withMessageColor(int resid)
                                .withDialogColor("#FFdde1ee")                               //def  | withDialogColor(int resid)
                                .withIcon(getResources().getDrawable(android.R.drawable.ic_menu_info_details))
                                .withDuration(700)                                          //def
                                .withEffect(Effectstype.Fliph)                                         //def Effectstype.Slidetop
                                .withButton1Text("Invite")                                      //def gone
                                .withButton2Text("Cancel")                                  //def gone
                                .isCancelableOnTouchOutside(true)                           //def    | isCancelable(true)
                                .setCustomView(et, RoomActivity.this)         //.setCustomView(View or ResId,context)
                                .setButton1Click(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                XMPPRequest.inviteUser(et.getText().toString());
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        dialogBuilder.dismiss();
                                                    }
                                                });
                                            }
                                        }).start();

                                    }
                                })
                                .setButton2Click(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogBuilder.dismiss();
                                    }
                                })
                                .show();
                        break;


                    default:
                        break;
                }
//                Toast.makeText(RoomActivity.this, "Do some thing here, index :" + index, Toast.LENGTH_LONG).show();

            }

        });

        ArrayList<String> faceNameList = new ArrayList<String>();
        for (int x = 1; x <= 10; x++) {
            faceNameList.add("big" + x);
        }
        for (int x = 1; x <= 10; x++) {
            faceNameList.add("big" + x);
        }

        ArrayList<String> faceNameList1 = new ArrayList<String>();
        for (int x = 1; x <= 7; x++) {
            faceNameList1.add("cig" + x);
        }


        ArrayList<String> faceNameList2 = new ArrayList<String>();
        for (int x = 1; x <= 24; x++) {
            faceNameList2.add("dig" + x);
        }

        Map<Integer, ArrayList<String>> faceData = new HashMap<Integer, ArrayList<String>>();
        faceData.put(R.drawable.em_cate_magic, faceNameList2);
        faceData.put(R.drawable.em_cate_rib, faceNameList1);
        faceData.put(R.drawable.em_cate_duck, faceNameList);
        box.setFaceData(faceData);


        List<Option> functionData = new ArrayList<Option>();
//        for (int x = 0; x < 5; x++) {
        Option takePhotoOption = new Option(this, "Take", R.drawable.take_photo);
        Option galleryOption = new Option(this, "Gallery", R.drawable.gallery);
        Option RecordOption = new Option(this, "Invite", R.drawable.btn_add);
        functionData.add(galleryOption);
        functionData.add(takePhotoOption);
        functionData.add(RecordOption);
//        }
        box.setFunctionData(functionData);
    }


    private void initListView() {
        showImage = (ImageView) findViewById(R.id.showImage);
        listView = (ListView) findViewById(R.id.messageListview);

        //create Data
//        Message message = new Message(Message.MSG_TYPE_TEXT, Message.MSG_STATE_SUCCESS, "Tom", "avatar", "Jerry", "avatar", "Hi", false, true, new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24) * 8));
//        Message message1 = new Message(Message.MSG_TYPE_TEXT, Message.MSG_STATE_SUCCESS, "Tom", "avatar", "Jerry", "avatar", "Hello World", true, true, new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24) * 8));
//        Message message2 = new Message(Message.MSG_TYPE_PHOTO, Message.MSG_STATE_SUCCESS, "Tom", "avatar", "Jerry", "avatar", "device_2014_08_21_215311", false, true, new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24) * 7));
//        Message message3 = new Message(Message.MSG_TYPE_TEXT, Message.MSG_STATE_SUCCESS, "Tom", "avatar", "Jerry", "avatar", "Haha", true, true, new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24) * 7));
//        Message message4 = new Message(Message.MSG_TYPE_FACE, Message.MSG_STATE_SUCCESS, "Tom", "avatar", "Jerry", "avatar", "big3", false, true, new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24) * 7));
//        Message message5 = new Message(Message.MSG_TYPE_FACE, Message.MSG_STATE_SUCCESS, "Tom", "avatar", "Jerry", "avatar", "big2", true, true, new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24) * 6));
//        Message message6 = new Message(Message.MSG_TYPE_TEXT, Message.MSG_STATE_FAIL, "Tom", "avatar", "Jerry", "avatar", "test send fail", true, false, new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24) * 6));
//        Message message7 = new Message(Message.MSG_TYPE_TEXT, Message.MSG_STATE_SENDING, "Tom", "avatar", "Jerry", "avatar", "test sending", true, true, new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24) * 6));

        List<Message> messages = new ArrayList<Message>();
//        messages.add(message);
//        messages.add(message1);
//        messages.add(message2);
//        messages.add(message3);
//        messages.add(message4);
//        messages.add(message5);
//        messages.add(message6);
//        messages.add(message7);

        adapter = new MessageAdapter(this, messages);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        listView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                box.hide();
                return false;
            }
        });
        adapter.setClickListener(new MessageAdapter.ClickListener() {
            @Override
            public void onClick(int tag, int position) {
                Log.e("setOnItemClickListener", "setOnItemClickListener============" + position);
                Message message = adapter.getData().get(position);
                if (message.getType() == (com.jialin.chat.Message.MSG_TYPE_PHOTO)) {
                    Picasso.with(RoomActivity.this).load(message.getContent()).into(showImage);
                    box.hide();
                    showImage.setVisibility(View.VISIBLE);
                    showImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showImage.setVisibility(View.GONE);
                        }
                    });
                } else if (message.getType() == (Message.MSG_TYPE_VOICE)) {
                    MediaPlayer mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(message.getContent());
                        mediaPlayer.prepare();
                        mediaPlayer.start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Log.e("setOnItemClickListener","setOnItemClickListener============"+position);
//                Message message = adapter.getData().get(position);
//                if (message.getType() == (com.jialin.chat.Message.MSG_TYPE_PHOTO)) {
//                    Picasso.with(RoomActivity.this).load(message.getContent()).into(showImage);
//                    showImage.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            showImage.setVisibility(View.GONE);
//                        }
//                    });
//                }else if (message.getType() == (Message.MSG_TYPE_VOICE)) {
//                    MediaPlayer mediaPlayer = new MediaPlayer();
//                    try {
//                        mediaPlayer.setDataSource(message.getContent());
//                        mediaPlayer.prepare();
//                        mediaPlayer.start();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//
//
//            }
//        });

    }


    private void createReplayMsg(Message message) {

        final Message reMessage = new Message(message.getType(), 1, "Tom", "avatar", "Jerry", "avatar",
                message.getType() == 0 ? "Re:" + message.getContent() : message.getContent(),
                false, true, new Date()
        );
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(1000 * (new Random().nextInt(3) + 1));
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            adapter.getData().add(reMessage);
                            listView.setSelection(listView.getBottom());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.i(TAG, "Connected!");
        if (service == null)
            return;
        xmppService = ((XmppConnectionServiceBinder) service).getService();
        if (xmppService == null)
            return;

        Log.e(TAG, "onServiceConnected-------------------------!");

        MultiUserChat muc = XMPPRequest.muc;
//        xmppService.getConnection().addPacketListener(new MessageListener(adapter),null);
        muc.addMessageListener(new MessageListener(adapter, listView, new MessageListener.RefreshListener() {
            @Override
            public void refresh() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        listView.setSelection(listView.getBottom());
                    }

                });
            }
        }));
        muc.addParticipantStatusListener(new ParticipantStatus());
        XMPPRequest.findMulitUser(XMPPRequest.muc);

    }


    @Override
    public void onServiceDisconnected(ComponentName name) {

    }


    protected void getImageFromCamera() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            Intent getImageByCamera = new Intent(
                    "android.media.action.IMAGE_CAPTURE");
            getImageByCamera.putExtra("crop", "true");
//            getImageByCamera.putExtra("aspectX", 1);// 裁剪框比例
//            getImageByCamera.putExtra("aspectY", 1);
            // getImageByCamera.putExtra("outputX", crop);// 输出图片大小
            // getImageByCamera.putExtra("outputY", crop);
            startActivityForResult(getImageByCamera,
                    REQUEST_CODE_CAPTURE_CAMEIA);
        } else {
            Toast.makeText(this, getString(R.string.no_sdcard),
                    Toast.LENGTH_LONG).show();
        }
    }

    protected void getImageFromAlbum() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        intent.putExtra("crop", "true");
//        intent.putExtra("aspectX", 1);// 裁剪框比例
//        intent.putExtra("aspectY", 1);
        // intent.putExtra("outputX", crop);// 输出图片大小
        // intent.putExtra("outputY", crop);
        startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CODE_PICK_IMAGE:
            case REQUEST_CODE_CAPTURE_CAMEIA:
                Uri uri = data.getData();
                if (uri == null) {
                    Bitmap mBitmap = (Bitmap) data.getExtras().get("data");
                    if (mBitmap != null) {
                        Bitmap mBitmap2 = compressImage(mBitmap);
//                        photo_item.setImageBitmap(mBitmap2);

                        selectedImagePath = TEMP_JPG_PATH;
                        uploadBitmap(selectedImagePath);
                    } else {
                        Toast.makeText(this, R.string.failed,
                                Toast.LENGTH_LONG).show();
                    }
                    break;
                }
                selectedImagePath = getPath(uri);
                if (selectedImagePath != null && !selectedImagePath.equals("")) {
//                    photo_item.setImageBitmap(compressImage(
//                            selectedImagePath));
                    compressImage(selectedImagePath);
                    uploadBitmap(selectedImagePath);
                } else {
                    Toast.makeText(this, R.string.failed,
                            Toast.LENGTH_LONG).show();
                }
                break;


            default:
                break;
        }


    }

    private void uploadBitmap(final String result) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                QiniuUploadUitls.getInstance().uploadImage(result, new QiniuUploadUitls.QiniuUploadUitlsListener() {

                    @Override
                    public void onSucess(String fileUrl) {
                        // TODO Auto-generated method stub
                        // showUrlImage(fileUrl);
                        XMPPRequest.sendImageMessage(result, fileUrl);
                        Log.e("onSucess", "fileUrl===" + fileUrl);
                    }

                    @Override
                    public void onProgress(int progress) {
                        // TODO Auto-generated method stub
                        //pbProgress.setProgress(progress);
                        Log.e("onProgress", "onProgress===" + progress);
                    }

                    @Override
                    public void onError(int errorCode, String msg) {
                        // TODO Auto-generated method stub
                        //showToast("errorCode=" + errorCode + ",msg=" + msg);
                        Log.e("onError", "onError===" + msg);
                    }
                });
            }
        }).start();
    }

    private void uploadRecord(final String result) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                QiniuUploadUitls.getInstance().uploadImage(result, new QiniuUploadUitls.QiniuUploadUitlsListener() {

                    @Override
                    public void onSucess(String fileUrl) {
                        // TODO Auto-generated method stub
                        // showUrlImage(fileUrl);
                        XMPPRequest.sendRecordMessage(result, fileUrl);
                        Log.e("onSucess", "fileUrl===" + fileUrl);
                    }

                    @Override
                    public void onProgress(int progress) {
                        // TODO Auto-generated method stub
                        //pbProgress.setProgress(progress);
                        Log.e("onProgress", "onProgress===" + progress);
                    }

                    @Override
                    public void onError(int errorCode, String msg) {
                        // TODO Auto-generated method stub
                        //showToast("errorCode=" + errorCode + ",msg=" + msg);
                        Log.e("onError", "onError===" + msg);
                    }
                });
            }
        }).start();
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null,
                null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    public Bitmap compressImage(Bitmap bitmap) {
        Bitmap mBitmap = compressImageData(bitmap);
        saveBitmapToFile(mBitmap, TEMP_JPG_PATH);
        if (!bitmap.isRecycled()) {
            bitmap.recycle();
        }
        return mBitmap;

    }

    public Bitmap compressImageData(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while (baos.toByteArray().length / 1024 > 1000) { // 循环判断如果压缩后图片是否大于100kb,大于继续压缩
            baos.reset();// 重置baos即清空baos
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中
            options -= 1;// 每次都减少10
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);// 把ByteArrayInputStream数据生成图片
        return bitmap;
    }

    /**
     * Save Bitmap to a file.保存图片到SD卡。
     *
     * @param bitmap
     * @param _file
     * @return error message if the saving is failed. null if the saving is
     * successful.
     * @throws IOException
     */
    public void saveBitmapToFile(Bitmap bitmap, String _file) {
        BufferedOutputStream os = null;
        try {
            File file = new File(_file);
            // String _filePath_file.replace(File.separatorChar +
            // file.getName(), "");
            int end = _file.lastIndexOf(File.separator);
            String _filePath = _file.substring(0, end);
            File filePath = new File(_filePath);
            if (!filePath.exists()) {
                filePath.mkdirs();
            }
            file.createNewFile();
            os = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    Log.e("Save image failed", e.getMessage(), e);
                }
            }
        }
    }


    public class MsgReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(DEF.ACTION_INVITE)) {
                final TextView et = new TextView(RoomActivity.this);
                et.setPadding(10, 10, 10, 10);
                et.setText(intent.getStringExtra(DEF.ACTION_INVITE));

                final NiftyDialogBuilder dialogBuilder1 = NiftyDialogBuilder.getInstance(RoomActivity.this);
                dialogBuilder1
                        .withTitle("Invite")                                  //.withTitle(null)  no title
                        .withTitleColor("#000000")                                  //def
                        .withDividerColor("#11ffffff")                           //def  | withMessageColor(int resid)
                        .withDialogColor("#FFdde1ee")                               //def  | withDialogColor(int resid)
                        .withIcon(getResources().getDrawable(android.R.drawable.ic_menu_info_details))
                        .withDuration(700)                                          //def
                        .withEffect(Effectstype.Fliph)                                         //def Effectstype.Slidetop
                        .withButton1Text("OK")                                   //def gone
                        .isCancelableOnTouchOutside(true)                           //def    | isCancelable(true)
                        .setCustomView(et, RoomActivity.this)         //.setCustomView(View or ResId,context)
                        .setButton1Click(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                dialogBuilder1.dismiss();
                                            }
                                        });

                                    }
                                }).start();

                            }
                        })
                        .show();
            }

        }

    }

    public Bitmap compressImage(String srcPath) {
        Log.e("test_pulque", "=compressImage=" + srcPath);
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        // 开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);// 此时返回bm为空

        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        // 现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        float hh = 400f;// 这里设置高度为800f
        float ww = 400f;// 这里设置宽度为480f
        // 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;// be=1表示不缩放
        if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;// 设置缩放比例
        // 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
        Bitmap mBitmap = compressImageData(bitmap);
        saveBitmapToFile(mBitmap, srcPath);
        if (!bitmap.isRecycled()) {
            bitmap.recycle();
        }
        return mBitmap;

    }

}
