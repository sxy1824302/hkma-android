package com.pulque.androidxmpp.entity;

/**
 * Created by Pulque on 2015/2/8.
 */
public class FriendRooms {

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getJid() {
        return Jid;
    }

    public void setJid(String jid) {
        Jid = jid;
    }

    public int getOccupants() {
        return Occupants;
    }

    public void setOccupants(int occupants) {
        Occupants = occupants;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    private String Name;
    private String Jid;
    private int Occupants;
    private String Description;
    private String Subject;

    @Override
    public String toString() {
        return "FriendRooms{" +
                "Name='" + Name + '\'' +
                ", Jid='" + Jid + '\'' +
                ", Occupants=" + Occupants +
                ", Description='" + Description + '\'' +
                ", Subject='" + Subject + '\'' +
                '}';
    }
}
