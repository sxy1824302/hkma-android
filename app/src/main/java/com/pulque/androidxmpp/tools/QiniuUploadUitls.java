package com.pulque.androidxmpp.tools;


import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;
import com.qiniu.api.auth.AuthException;
import com.qiniu.api.auth.digest.Mac;
import com.qiniu.api.rs.PutPolicy;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import static android.util.Base64.encode;


/**
 * Created by Pulque.Li on 2/16/2015.
 */
public class QiniuUploadUitls {

    /** 在网站上查看 */
    private static final String ACCESS_KEY = "4zU35SpqfbaOjDmzzUeglUxdlvBkNDEMTK34g6n_";
    /** 在网站上查看 */
    private static final String SECRET_KEY = "8LuieIu1y_fTDKyV6lsxwfyCkEQtJnMr9s8lKdTY";
    /** 你所创建的空间的名称*/
    private static final String bucketName = "pulque";

    private static final String fileName = "temp.jpg";

    private static final String tempJpeg = Environment
            .getExternalStorageDirectory().getPath() + "/" + fileName;

    private int maxWidth = 720;
    private int maxHeight = 1080;

    public interface QiniuUploadUitlsListener{
        public void onSucess(String fileUrl);
        public void onError(int errorCode,String msg);
        public void onProgress(int progress);
    }

    private QiniuUploadUitls() {

    }

    private static QiniuUploadUitls qiniuUploadUitls = null;

    private UploadManager uploadManager = new UploadManager();

    public static QiniuUploadUitls getInstance() {
        if (qiniuUploadUitls == null) {
            qiniuUploadUitls = new QiniuUploadUitls();
        }
        return qiniuUploadUitls;
    }

    public boolean saveBitmapToJpegFile(Bitmap bitmap, String filePath) {
        return saveBitmapToJpegFile(bitmap, filePath, 75);
    }

    public boolean saveBitmapToJpegFile(Bitmap bitmap, String filePath,
                                        int quality) {
        try {
            FileOutputStream fileOutStr = new FileOutputStream(filePath);
            BufferedOutputStream bufOutStr = new BufferedOutputStream(
                    fileOutStr);
            resizeBitmap(bitmap).compress(CompressFormat.JPEG, quality, bufOutStr);
            bufOutStr.flush();
            bufOutStr.close();
        } catch (Exception exception) {
            return false;
        }
        return true;
    }

    /**
     * 缩小图片
     * @param bitmap
     * @return
     */
    public Bitmap resizeBitmap(Bitmap bitmap) {
        if(bitmap != null){
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if(width>maxWidth){
                int pWidth = maxWidth;
                int pHeight = maxWidth*height/width;
                Bitmap result = Bitmap.createScaledBitmap(bitmap, pWidth, pHeight, false);
                bitmap.recycle();
                return result;
            }
            if(height>maxHeight){
                int pHeight = maxHeight;
                int pWidth = maxHeight*width/height;
                Bitmap result = Bitmap.createScaledBitmap(bitmap, pWidth, pHeight, false);
                bitmap.recycle();
                return result;
            }
        }
        return bitmap;
    }

    public void uploadImage(Bitmap bitmap,QiniuUploadUitlsListener listener) {
        saveBitmapToJpegFile(bitmap, tempJpeg);
        uploadImage(tempJpeg,listener);
    }

    public void uploadImage(String filePath,final QiniuUploadUitlsListener listener) {
        final String fileUrlUUID = getFileUrlUUID();
        String token = getToken();
        if (token == null) {
            if(listener != null){
                listener.onError(-1, "token is null");
            }
            return;
        }
        uploadManager.put(filePath, fileUrlUUID, token,
                new UpCompletionHandler() {
                    @Override
                    public void complete(String key, ResponseInfo info,
                                         JSONObject response) {
                        System.out.println("debug:info = " + info
                                + ",response = " + response);
                        if (info != null && info.statusCode == 200) {// 上传成功
                            String fileRealUrl = getRealUrl(fileUrlUUID);
                            System.out.println("debug:fileRealUrl = "+fileRealUrl);
                            if(listener != null){
                                listener.onSucess(fileRealUrl);
                            }
                        } else {
                            if(listener != null){
                                listener.onError(info.statusCode, info.error);
                            }
                        }
                    }
                }, new UploadOptions(null, null, false,
                        new UpProgressHandler() {
                            public void progress(String key, double percent) {
                                if(listener != null){
                                    listener.onProgress((int)(percent*100));
                                }
                            }
                        }, null));

    }

    /**
     * 生成远程文件路径（全局唯一）
     *
     * @return
     */
    private String getFileUrlUUID() {
        String filePath = android.os.Build.MODEL + "__"
                + System.currentTimeMillis() + "__"
                + (new Random().nextInt(500000)) + "_"
                + (new Random().nextInt(10000));
        return filePath.replace(".", "0");
    }

    private String getRealUrl(String fileUrlUUID){
        String filePath = "http://7vikl4.com1.z0.glb.clouddn.com/"+fileUrlUUID;
        return filePath;
    }

    /**
     * 获取token 本地生成
     *
     * @return
     */
    private String getToken() {
        Mac mac = new Mac(ACCESS_KEY, SECRET_KEY);
        PutPolicy putPolicy = new PutPolicy(bucketName);
        putPolicy.returnBody = "{\"name\": $(fname),\"size\": \"$(fsize)\",\"w\": \"$(imageInfo.width)\",\"h\": \"$(imageInfo.height)\",\"key\":$(etag)}";
        try {
            String uptoken = putPolicy.token(mac);
            System.out.println("debug:uptoken = " + uptoken);
            return uptoken;
        } catch (AuthException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        JSONObject mJsonObject = new JSONObject();
//        try {
//            mJsonObject.put("scope", bucketName);
//            mJsonObject.put("deadline", new Date().getTime()+100000000);
//            Log.e("mJsonObject","mJsonObject==="+mJsonObject.toString());
////            String b64 = new String(Base64.decode(mJsonObject.toString(),Base64.DEFAULT));
//            String b64 =  encodeForUrl(mJsonObject.toString().getBytes());
//            Log.e("b64","b64==="+b64);
//            String encoded_signed =  encodeForUrl(HmacSHA1Encrypt(b64,SECRET_KEY ));
//
////            new String(Base64.decode(,Base64.DEFAULT));
//            Log.e("encoded_signed","encoded_signed==="+encoded_signed);
//            String rel = ACCESS_KEY+":"+encoded_signed+":"+b64;
//            return rel;
//        } catch (JSONException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        return null;
    }

    public static String encodeForUrl(byte[] s){
        if (s == null)
            return null;
        String standerBase64 = new String(Base64.encode(s, Base64.DEFAULT));
        String encodeForUrl = standerBase64;
        //转成针对url的base64编码
        encodeForUrl = encodeForUrl.replace("=", "");
        encodeForUrl = encodeForUrl.replace("+", "*");
        encodeForUrl = encodeForUrl.replace("/", "-");
        //去除换行
        encodeForUrl = encodeForUrl.replace("\n", "");
        encodeForUrl = encodeForUrl.replace("\r", "");

        //转换*号为 -x-
        //防止有违反协议的字符
        encodeForUrl = encodeSpecialLetter1(encodeForUrl);

        return encodeForUrl;

    }

    /**
     * 转换*号为 -x-，
     为了防止有违反协议的字符，-x 转换为-xx
     * @param str
     * @return
     */
    private static String encodeSpecialLetter1(String str){
        str = str.replace("-x", "-xx");
        str = str.replace("*", "-x-");
        return str;
    }



    private static final String MAC_NAME = "HmacSHA1";
    private static final String ENCODING = "UTF-8";
    /**
     * 使用 HMAC-SHA1 签名方法对对encryptText进行签名
     * @param encryptText 被签名的字符串
     * @param encryptKey  密钥
     * @return
     * @throws Exception
     */
    public static byte[] HmacSHA1Encrypt(String encryptText, String encryptKey) throws Exception
    {
        byte[] data=encryptKey.getBytes(ENCODING);
        //根据给定的字节数组构造一个密钥,第二参数指定一个密钥算法的名称
        SecretKey secretKey = new SecretKeySpec(data, MAC_NAME);
        //生成一个指定 Mac 算法 的 Mac 对象
        javax.crypto.Mac mac = javax.crypto.Mac.getInstance(MAC_NAME);
        //用给定密钥初始化 Mac 对象
        mac.init(secretKey);

        byte[] text = encryptText.getBytes(ENCODING);
        //完成 Mac 操作
        return mac.doFinal(text);
    }

}
