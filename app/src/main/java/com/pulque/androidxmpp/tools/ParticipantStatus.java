package com.pulque.androidxmpp.tools;

/**
 * Created by Pulque on 2015/2/11.
 */

import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.muc.ParticipantStatusListener;

/**
 * 会议室状态监听事件
 *
 * @author Administrator
 *
 */
public class ParticipantStatus implements ParticipantStatusListener {

    @Override
    public void adminGranted(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void adminRevoked(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void banned(String arg0, String arg1, String arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void joined(String participant) {
        System.out.println(StringUtils.parseResource(participant)+ " has joined the room.");
    }

    @Override
    public void kicked(String arg0, String arg1, String arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void left(String participant) {
        // TODO Auto-generated method stub
        System.out.println(StringUtils.parseResource(participant)+ " has left the room.");

    }

    @Override
    public void membershipGranted(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void membershipRevoked(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void moderatorGranted(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void moderatorRevoked(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void nicknameChanged(String participant, String newNickname) {
        System.out.println(StringUtils.parseResource(participant)+ " is now known as " + newNickname + ".");
    }

    @Override
    public void ownershipGranted(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void ownershipRevoked(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void voiceGranted(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void voiceRevoked(String arg0) {
        // TODO Auto-generated method stub

    }


}
