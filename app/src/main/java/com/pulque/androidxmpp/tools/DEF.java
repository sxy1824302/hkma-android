package com.pulque.androidxmpp.tools;

/**
 * Created by Pulque.Li on 2/13/2015.
 */
public class DEF {

    public static final String OAUTH_URL = "https://acc.qbox.me/oauth2/token";
    public static String DOMAIN = "kazytommy.dlinkddns.com";
    public static String RESOURCE = "AndroidXMPP";


    public static String NOT_RESULT = "The server does not return a result.";
    public static String ALREADY_EXISTS = "This account already exists.";
    public static String REGISTRATION_FAILED = "Registration Failed.";
    public static String SUCCESSFUL_REGISTRATION = "Congratulations on your successful registration.";

    public static final String ACTION_RECEIVER = "ACTION.RECEIVER";
    public static final String ACTION_LOGIN = "ACTION.LOGIN";
    public static final String ACTION_ISCONNECTED_FAILED = "isConnected_failed";
    public static final String ACTION_INVITE = "ACTION_INVITE";

    public static final int SHOW_PD_LOADING = 0;
    public static final int SHOW_PD_SUBMITTING = SHOW_PD_LOADING + 1;
    public static final int SHOW_PD_LOGIN = SHOW_PD_SUBMITTING + 1;
}
