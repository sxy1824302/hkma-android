package com.pulque.androidxmpp.tools;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.Toast;

import com.pulque.androidxmpp.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pulque.Li on 2/13/2015.
 */
public class Utils {

    private static ProgressDialog pd;

    /*
     * tag: Constant.SHOWPD_LOADING Constant.SHOWPD_SUBMITTING
     * Constant.SHOWPD_LOGIN
     */
    public static void showPD(Context mContext, int tag) {
        if (pd == null) {
            String mes = "";
            switch (tag) {
                case DEF.SHOW_PD_LOADING:
                    mes = mContext.getString(R.string.Loading);
                    break;
                case DEF.SHOW_PD_SUBMITTING:
                    mes = mContext.getString(R.string.Submitting);
                    break;
                case DEF.SHOW_PD_LOGIN:
                    mes = mContext.getString(R.string.login_in_progress);
                    break;

                default:
                    break;
            }
            pd = ProgressDialog.show(mContext, "", mes, false, true);
        } else if (!pd.isShowing()) {
            pd.show();
        }
    }

    public static void dismissPD() {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
            pd = null;
        }
    }


    public static void showToast(Context context, String message) {
        Toast center = Toast.makeText(context, message, Toast.LENGTH_LONG);
        center.setGravity(Gravity.CENTER, 0, 0);
        center.show();
    }

    public static void showToast(Context context, int message_id) {
        Toast center = Toast.makeText(context, message_id, Toast.LENGTH_LONG);
        center.setGravity(Gravity.CENTER, 0, 0);
        center.show();
    }

    public static int dp2px(Context context, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics());
    }




    public static String executeLoginPost() {
        String result = null;
        BufferedReader reader = null;
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost request = new HttpPost();
            request.setURI(new URI(DEF.OAUTH_URL));
            List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            postParameters
                    .add(new BasicNameValuePair("grant_type", "password"));
            postParameters.add(new BasicNameValuePair("username", "lizhe0052@126.com"));
            postParameters.add(new BasicNameValuePair("password", "1qwe32"));
            postParameters.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded"));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(
                    postParameters);
            request.setEntity(formEntity);

            HttpParams params = client.getParams();
            ConnManagerParams.setTimeout(params, 30000);
            HttpConnectionParams.setConnectionTimeout(params, 30000);
            HttpConnectionParams.setSoTimeout(params, 30000);

            HttpResponse response = client.execute(request);
            reader = new BufferedReader(new InputStreamReader(response
                    .getEntity().getContent()));

            StringBuffer strBuffer = new StringBuffer("");
            String line = null;
            while ((line = reader.readLine()) != null) {
                strBuffer.append(line);
            }
            result = strBuffer.toString();
            Log.e("test_pulque", "token=" + result);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                    reader = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }
}
