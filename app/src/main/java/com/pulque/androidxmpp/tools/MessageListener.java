package com.pulque.androidxmpp.tools;

/**
 * Created by Pulque on 2015/2/11.
 */

import android.text.TextUtils;
import android.util.Log;
import android.widget.ListView;

import com.jialin.chat.MessageAdapter;
import com.pulque.androidxmpp.service.XMPPRequest;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 會議室信息監聽事件
 *
 * @author Administrator
 *         <p/>
 *         <p/>
 *         <message to="androidddd@conference.mobileserver" type="groupchat"><body>{"data":"v 火锅","type":"text"}</body></message>
 *         [2/12/2015 12:54:10 PM] 孙翔宇IOS: {"data":"[害羞]","type":"text"}</body></message>
 *         [2/12/2015 12:55:00 PM] 孙翔宇IOS: {"data":"🏨","type":"text"}
 *         <p/>
 *         <p/>
 *         NSDictionary *jsonDic = @{@"type"   : @"voice",
 * @"data" : @{
 * @"time" : [NSNumber numberWithInteger:time],
 * @"url" :  url
 * }
 * };
 * [10:48:20 AM] 孙翔宇IOS:     NSDictionary *jsonDic = @{@"type"   : @"image",
 * @"data" : @[@{
 * @"width" : [NSNumber numberWithFloat:width],
 * @"height" : [NSNumber numberWithFloat:height],
 * @"url" :  url
 * }]
 */
public class MessageListener implements PacketListener {
    private final RefreshListener mListener;
    private List<com.jialin.chat.Message> MessageList;
    private MessageAdapter mAdapter;
    private ListView listView;

    public MessageListener(MessageAdapter mAdapter, ListView listView, RefreshListener mListener) {
        this.MessageList = mAdapter.getData();
        this.mAdapter = mAdapter;
        this.listView = listView;
        this.mListener = mListener;
    }

    @Override
    public void processPacket(Packet packet) {

        Message message = (Message) packet;
        Log.e("processPacket", "message==" + message);
        Log.e("processPacket", "message==" + message.getBody());
        // 接收来自聊天室的聊天信息
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

//        mh.setUserAccount(account);
//        String from = StringUtils.parseResource(message.getFrom());
//        String fromRoomName = StringUtils.parseName(message.getFrom());
//        mh.setMhRoomName(fromRoomName);
//        mh.setFriendAccount(from);
//        mh.setMhInfo(message.getBody());
//        mh.setMhTime(time);
//        mh.setMhType("left");
//        MessageList.add(mh);
//        mAdapter.notifyDataSetChanged();

//        JSONObject data = null;
//        String type = "";
//        String url = "";
        try {
            JSONObject mJsonObject = new JSONObject(message.getBody());
//            data = mJsonObject.optJSONObject("data");
            String type = mJsonObject.optString("type");
            //url = mJsonObject.optString("url");

            if (type.equals("text")) {
                com.jialin.chat.Message mh = new com.jialin.chat.Message();
                mh.setContent(mJsonObject.optString("data"));
                mh.setType(com.jialin.chat.Message.MSG_TYPE_TEXT);
                mh.setSendSucces(true);
                mh.setState(com.jialin.chat.Message.MSG_STATE_SUCCESS);
                String from = StringUtils.parseResource(message.getFrom()).replace("@mobileserver/AndroidXMPP","");
                Log.e("from", "from==" + from);
                if (from.contains(XMPPRequest.MyName))
                    mh.setIsSend(true);
                else
                    mh.setIsSend(false);

                mh.setFromUserName(from);
                mh.setTime(new Date());

                MessageList.add(mh);
                mListener.refresh();
            } else if (type.equals("image")) {
                JSONArray mJsonArray = mJsonObject.optJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    com.jialin.chat.Message mh = new com.jialin.chat.Message();
                    JSONObject mObject = mJsonArray.getJSONObject(i);
                    mh.setContent(mObject.optString("url"));
                    Log.e("url", "url============" + mObject.optString("url"));
                    mh.setType(com.jialin.chat.Message.MSG_TYPE_PHOTO);
                    mh.setSendSucces(true);
                    mh.setState(com.jialin.chat.Message.MSG_STATE_SUCCESS);
                    String from = StringUtils.parseResource(message.getFrom()).replace("@mobileserver/AndroidXMPP","");
                    Log.e("from", "from==" + from);
                    if (from.contains(XMPPRequest.MyName))
                        mh.setIsSend(true);
                    else
                        mh.setIsSend(false);

                    mh.setFromUserName(from);
                    mh.setTime(new Date());

                    MessageList.add(mh);
                    mListener.refresh();
                }

            } else if (type.equals("voice")) {
                JSONObject mObject = mJsonObject.optJSONObject("data");
                com.jialin.chat.Message mh = new com.jialin.chat.Message();
                mh.setContent(mObject.optString("url"));
                Log.e("url", "url============" + mObject.optString("url"));
                mh.setType(com.jialin.chat.Message.MSG_TYPE_VOICE);
                mh.setSendSucces(true);
                mh.setState(com.jialin.chat.Message.MSG_STATE_SUCCESS);
                String from = StringUtils.parseResource(message.getFrom()).replace("@mobileserver/AndroidXMPP","");
                Log.e("from", "from==" + from);
                if (from.contains(XMPPRequest.MyName))
                    mh.setIsSend(true);
                else
                    mh.setIsSend(false);

                mh.setFromUserName(from);
                mh.setTime(new Date());

                MessageList.add(mh);
                mListener.refresh();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
////        this.type = type;
////        this.state = state;
//        ??????this.fromUserName = fromUserName;
////        this.fromUserAvatar = fromUserAvatar;
//        ????this.toUserName = toUserName;
//        ????this.toUserAvatar = toUserAvatar;
////        this.content = content;
////        this.isSend = isSend;
////        this.sendSucces = sendSucces;
////        this.time = time;

//        com.jialin.chat.Message message = new com.jialin.chat.Message(com.jialin.chat.Message.MSG_TYPE_TEXT, com.jialin.chat.Message.MSG_STATE_SUCCESS, "Tom", "avatar", "Jerry", "avatar", "Hi", false, true, new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24) * 8));

    }

    public interface RefreshListener {
        public void refresh();
    }


}
