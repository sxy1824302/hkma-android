package com.pulque.androidxmpp.service;

import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.util.Log;

import com.jialin.chat.Message;
import com.pulque.androidxmpp.entity.FriendRooms;

import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.FormField;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.HostedRoom;
import org.jivesoftware.smackx.muc.InvitationRejectionListener;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.RoomInfo;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Pulque on 2015/2/8.
 * <p/>
 * http://blog.csdn.net/h7870181/article/details/8737765
 *
 * http://blog.csdn.net/lnb333666/article/details/7598869
 */
public class XMPPRequest {

    public static MultiUserChat muc;
    public static String MyName;

    /**
     * 创建房间
     *
     * @param roomName 房间名称
     */
    public static void createRoom(XMPPConnection connection, String roomName) {
        if (connection == null) {
            return;
        }
        try {
            Collection<HostedRoom> hostrooms = MultiUserChat.getHostedRooms(connection,
                    "");//conference.zhanghaitao-pc
            for (HostedRoom entry : hostrooms) {
                System.out.println(entry.getName() + " - " + entry.getJid());
//                roominfos.add(entry);
            }
            Log.e("createRoom", "====" + roomName
                    + "@conference." + connection.getServiceName());
            // 创建一个MultiUserChat
            muc = new MultiUserChat(connection, roomName
                    + "@conference." + connection.getServiceName());
            // 创建聊天室
            muc.create(roomName); // roomName房间的名字
            // 获得聊天室的配置表单
            Form form = muc.getConfigurationForm();
            // 根据原始表单创建一个要提交的新表单。
            Form submitForm = form.createAnswerForm();
            // 向要提交的表单添加默认答复
            for (Iterator<FormField> fields = form.getFields(); fields
                    .hasNext(); ) {
                FormField field = (FormField) fields.next();
                if (!FormField.TYPE_HIDDEN.equals(field.getType())
                        && field.getVariable() != null) {
                    // 设置默认值作为答复
                    submitForm.setDefaultAnswer(field.getVariable());
                }
            }
            // 设置聊天室的新拥有者
            List<String> owners = new ArrayList<String>();
            Log.e("owners", "connection.getUser()===" + connection.getUser());
            owners.add(connection.getUser());// 用户JID
            submitForm.setAnswer("muc#roomconfig_roomowners", owners);
            // 设置聊天室是持久聊天室，即将要被保存下来
            submitForm.setAnswer("muc#roomconfig_persistentroom", true);
            // 房间仅对成员开放
            submitForm.setAnswer("muc#roomconfig_membersonly", false);
            // 允许占有者邀请其他人
            submitForm.setAnswer("muc#roomconfig_allowinvites", true);
            // 进入是否需要密码
            //submitForm.setAnswer("muc#roomconfig_passwordprotectedroom", true);
            // 设置进入密码
            //submitForm.setAnswer("muc#roomconfig_roomsecret", "password");
            // 能够发现占有者真实 JID 的角色
//            submitForm.setAnswer("muc#roomconfig_whois", "anyone");
            // 登录房间对话
            submitForm.setAnswer("muc#roomconfig_enablelogging", true);
            // 仅允许注册的昵称登录
            submitForm.setAnswer("x-muc#roomconfig_reservednick", false);
            // 允许使用者修改昵称
            submitForm.setAnswer("x-muc#roomconfig_canchangenick", false);
            // 允许用户注册房间
            submitForm.setAnswer("x-muc#roomconfig_registration", true);
            // 发送已完成的表单（有默认值）到服务器来配置聊天室
            submitForm.setAnswer("muc#roomconfig_passwordprotectedroom", true);
            // 发送已完成的表单（有默认值）到服务器来配置聊天室
            muc.sendConfigurationForm(submitForm);
        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }


    /**
     * 加入会议室
     *
     * @param user       昵称
     * @param password   会议室密码
     * @param roomsName  会议室名
     * @param connection
     */
    public static MultiUserChat joinMultiUserChat(String user, String password, String roomsName,
                                                  XMPPConnection connection) {
        try {
            MyName = connection.getUser().replace("@mobileserver/AndroidXMPP", "");
            // 使用XMPPConnection创建一个MultiUserChat窗口
            muc = new MultiUserChat(connection, roomsName
                    + "@conference." + connection.getServiceName());
            // 聊天室服务将会决定要接受的历史记录数量
            DiscussionHistory history = new DiscussionHistory();
            history.setMaxStanzas(0);
            //history.setSince(new Date());
            // 用户加入聊天室
            muc.join(user, password, history, SmackConfiguration.getPacketReplyTimeout());
            System.out.println("会议室加入成功........");
            return muc;
        } catch (XMPPException e) {
            e.printStackTrace();
            System.out.println("会议室加入失败........");
            return null;
        }
    }


    /**
     * 查询会议室成员名字MultiUserChat muc
     */
    public static List<String> findMulitUser(MultiUserChat muc) {
        List<String> listUser = new ArrayList<String>();
        Iterator<String> it = muc.getOccupants();
        //遍历出聊天室人员名称
        while (it.hasNext()) {
            // 聊天室成员名字
            String name = StringUtils.parseResource(it.next());
            listUser.add(name);
            Log.e("findMulitUser","findMulitUser==="+name);
        }
        return listUser;
    }


    /**
     * 获取服务器上所有会议室
     *
     * @return
     * @throws org.jivesoftware.smack.XMPPException
     */
    public static List<FriendRooms> getConferenceRoom(XMPPConnection connection) throws XMPPException {
        List<FriendRooms> list = new ArrayList<FriendRooms>();
        new ServiceDiscoveryManager(connection);
        if (!MultiUserChat.getHostedRooms(connection,
                connection.getServiceName()).isEmpty()) {

            for (HostedRoom k : MultiUserChat.getHostedRooms(connection,
                    connection.getServiceName())) {

                for (HostedRoom j : MultiUserChat.getHostedRooms(connection,
                        k.getJid())) {
                    RoomInfo info2 = MultiUserChat.getRoomInfo(connection,
                            j.getJid());

                    Log.e("getConferenceRoom", "=========================================");
                    Log.e("getConferenceRoom", "getDescription!==" + info2.getDescription());
                    Log.e("getConferenceRoom", "getRoom!==" + info2.getRoom());
                    Log.e("getConferenceRoom", "getSubject!==" + info2.getSubject());
                    Log.e("getConferenceRoom", "getOccupantsCount!==" + info2.getOccupantsCount());
                    Log.e("getConferenceRoom", "getJid!==" + j.getJid());
                    Log.e("getConferenceRoom", "getName!==" + j.getName());
                    if (j.getJid().indexOf("@") > 0) {

                        FriendRooms friendrooms = new FriendRooms();
                        friendrooms.setName(j.getName());//聊天室的名称
                        friendrooms.setJid(j.getJid());//聊天室JID
                        friendrooms.setOccupants(info2.getOccupantsCount());//聊天室中占有者数量
                        friendrooms.setDescription(info2.getDescription());//聊天室的描述
                        friendrooms.setSubject(info2.getSubject());//聊天室的主题
                        list.add(friendrooms);
                    }
                }
            }
        }
        Log.e("getConferenceRoom", "=========================================");
        Log.e("getConferenceRoom", "list!==" + list.size());
        return list;
    }

    public static Message sendMessage(String content) {
        if (muc != null) {
            try {
                JSONObject mJsonObject = new JSONObject();
                mJsonObject.put("type", "text");
                mJsonObject.put("data", content);
                Log.e("sendMessage", "sendMessage ===" + mJsonObject.toString());
                org.jivesoftware.smack.packet.Message message = muc.createMessage();
                message.setBody(mJsonObject.toString());
                muc.sendMessage(message);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (XMPPException e) {
                e.printStackTrace();
            }
            Message message = new Message(Message.MSG_TYPE_TEXT, Message.MSG_STATE_SUCCESS, "", "", "", "", content, true, true, new Date());
            return message;
        }
        return null;
    }

    public static Message sendImageMessage(String uri, String content) {
        if (muc != null) {
            try {

                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(uri, opts);
                opts.inSampleSize = 1;
                opts.inJustDecodeBounds = false;

                int width = opts.outWidth;
                int height = opts.outHeight;

                JSONObject mJsonObject1 = new JSONObject();
                mJsonObject1.put("width", width);
                mJsonObject1.put("height", height);
                mJsonObject1.put("url", content);
                JSONArray mJsonArray = new JSONArray();
                mJsonArray.put(mJsonObject1);
                JSONObject mJsonObject = new JSONObject();
                mJsonObject.put("type", "image");
                mJsonObject.put("data",mJsonArray);

                Log.e("sendMessage", "sendMessage ===" + mJsonObject.toString());
                org.jivesoftware.smack.packet.Message message = muc.createMessage();
                message.setBody(mJsonObject.toString());
                muc.sendMessage(message);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (XMPPException e) {
                e.printStackTrace();
            }
            Message message = new Message(Message.MSG_TYPE_TEXT, Message.MSG_STATE_SUCCESS, "", "", "", "", content, true, true, new Date());
            return message;
        }
        return null;
    }public static Message sendRecordMessage(String uri, String content) {
        if (muc != null) {
            try {
                MediaPlayer mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(uri);
                mediaPlayer.prepare();

                JSONObject mJsonObject1 = new JSONObject();
                mJsonObject1.put("time", mediaPlayer.getDuration()/1000.0f);
                mJsonObject1.put("url", content);
                JSONObject mJsonObject = new JSONObject();
                mJsonObject.put("type", "voice");
                mJsonObject.put("data",mJsonObject1);

                Log.e("sendMessage", "sendMessage ===" + mJsonObject.toString());
                org.jivesoftware.smack.packet.Message message = muc.createMessage();
                message.setBody(mJsonObject.toString());
                muc.sendMessage(message);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (XMPPException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Message message = new Message(Message.MSG_TYPE_TEXT, Message.MSG_STATE_SUCCESS, "", "", "", "", content, true, true, new Date());
            return message;
        }
        return null;
    }

    public static void inviteUser(String name){
        try {
            if (muc != null) {
                muc.addInvitationRejectionListener(new InvitationRejectionListener() {
                    @Override
                    public void invitationDeclined(String s, String s2) {
                        Log.e("addInvitation", "invitationDeclined ===" + s+"  "+s2);
                    }
                });
                muc.invite(name+"@mobileserver/AndroidXMPP", "Invite you to my chat room.");
                muc.invite(name, "Invite you to my chat room.");
            }
        }catch (Exception e){

        }

    }
}
