package com.pulque.androidxmpp.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.pulque.androidxmpp.R;
import com.pulque.androidxmpp.activity.RoomListActivity;
import com.pulque.androidxmpp.tools.DEF;
import com.pulque.androidxmpp.tools.Utils;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Registration;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.search.UserSearch;

public class XmppConnectionService extends Service implements IXmppConnectionService {

    private static final String TAG = "XmppConnectionService";

    private XmppConnectionServiceBinder binder;

    private XMPPConnection m_connection;

    public static final String TAG_SERVICE = "tag_service";
    public static final int TAG_LOGIN = 0;
    public static final int TAG_REGISTER = TAG_LOGIN + 1;


    public static final String SERVER_HOST = "serverHost";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String RESOURCE = "resource";
    public static final String RESULT = "result";

    @Override
    public void onCreate() {
        super.onCreate();
        binder = new XmppConnectionServiceBinder(this);
        Log.d(this.getClass().getName(), "onCreate");
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Log.d(this.getClass().getName(), "onStart");
        if (intent == null){
            return START_STICKY;
        }
        switch (intent.getIntExtra(TAG_SERVICE, 10000)) {
            case TAG_LOGIN:
                new Thread() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        super.run();
                        String serverHost = intent.getStringExtra(SERVER_HOST);
                        String login = intent.getStringExtra(LOGIN);
                        String password = intent.getStringExtra(PASSWORD);
                        String resource = intent.getStringExtra(RESOURCE);
                        try {
                            connection(serverHost, login, password, resource);
                        } catch (XMPPException e) {
                            Log.e(TAG, "Unable to connect to xmpp :", e);
                            Log.e(TAG, "Unable to connect to xmpp :" + e.toString());
                            String error = e.toString();
                            if (!TextUtils.isEmpty(error) && error.contains("502")) {
                                Intent openIntent = new Intent(DEF.ACTION_ISCONNECTED_FAILED);
                                sendBroadcast(openIntent);
                            } else {
                                Intent openIntent = new Intent(DEF.ACTION_LOGIN);
                                sendBroadcast(openIntent);
                            }
                            return;
                        }
                        Log.e(TAG, "login :" + login);
                        Intent openIntent = new Intent(getBaseContext(), RoomListActivity.class);
                        openIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getApplication().startActivity(openIntent);
                        Utils.dismissPD();
                    }
                }.start();
                break;
            case TAG_REGISTER:
                new Thread() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        super.run();
                        String serverHost = intent.getStringExtra(SERVER_HOST);
                        String login = intent.getStringExtra(LOGIN);
                        String password = intent.getStringExtra(PASSWORD);
                        Log.e(TAG, "serverHost :" + serverHost);
                        Log.e(TAG, "login :" + login);
                        Log.e(TAG, "password :" + password);
                        String mString = "";
                        try {
                            mString = register(serverHost, login, password);
                        } catch (XMPPException e) {
                            //Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "Unable to connect to xmpp :", e);
                        }

                        Intent openIntent = new Intent(DEF.ACTION_RECEIVER);
                        openIntent.putExtra(RESULT, mString);
                        sendBroadcast(openIntent);
                    }
                }.start();

                break;
        }


        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(this.getClass().getName(), "onDestroy");
        disconnect();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    /**
     * Initialisation de la connexion
     */
    public String register(String serverHost, String login, String password) throws XMPPException {

        ConnectionConfiguration config = new ConnectionConfiguration(serverHost);
        m_connection = new XMPPConnection(config);
        m_connection.connect();
//        m_connection.login(login, password, resource);
//        Presence presence = new Presence(Presence.Type.available);
//        m_connection.sendPacket(presence);
//        ProviderManager pm = ProviderManager.getInstance();
//        configure(pm);

        Registration reg = new Registration();
        reg.setType(IQ.Type.SET);
        reg.setTo(m_connection.getServiceName());
        reg.setUsername(login);
        reg.setPassword(password);
        reg.addAttribute("android", "createUser_android");
        System.out.println("reg:" + reg);
        PacketFilter filter = new AndFilter(new PacketIDFilter(reg
                .getPacketID()), new PacketTypeFilter(IQ.class));
        PacketCollector collector = m_connection
                .createPacketCollector(filter);
        m_connection.sendPacket(reg);

        IQ result = (IQ) collector.nextResult(SmackConfiguration
                .getPacketReplyTimeout());
        // Stop queuing results
        collector.cancel();
        String mString = "";
        if (result == null) {
            mString = DEF.NOT_RESULT;
        } else if (result.getType() == IQ.Type.ERROR) {
            if (result.getError().toString().equalsIgnoreCase(
                    "conflict(409)")) {
                mString = DEF.ALREADY_EXISTS;
            } else {
                mString = DEF.REGISTRATION_FAILED;
            }
        } else if (result.getType() == IQ.Type.RESULT) {
            mString = DEF.SUCCESSFUL_REGISTRATION;
        }
        return mString;
    }

    /**
     * Initialisation de la connexion
     */
    public void connection(String serverHost, String login, String password, String resource) throws XMPPException {
        ConnectionConfiguration config = new ConnectionConfiguration(serverHost);
        m_connection = new XMPPConnection(config);
        m_connection.connect();
        m_connection.login(login, password, resource);
        Presence presence = new Presence(Presence.Type.available);
        m_connection.sendPacket(presence);
        ProviderManager pm = ProviderManager.getInstance();
        configure(pm);
        Log.e("connection", "connection=========" + m_connection.getUser());
        MultiUserChat.addInvitationListener(m_connection,
                new InvitationListener() {
                    // 对应参数：连接、 房间JID、房间名、附带内容、密码、消息
                    @Override
                    public void invitationReceived(Connection connection, final String s, final String s2, final String s3, String s4, org.jivesoftware.smack.packet.Message message) {
                        Log.e("addInvitationListener", "addInvitationListener=========" + s);
                        Log.e("addInvitationListener", "addInvitationListener=========" + s2);
                        Log.e("addInvitationListener", "addInvitationListener=========" + s3);
                        Log.e("addInvitationListener", "addInvitationListener=========" + s4);

                        Intent openIntent = new Intent(DEF.ACTION_INVITE);
                        openIntent.putExtra(DEF.ACTION_INVITE,"You received an invitation from " + s2.substring(0, s2.indexOf("@")) + " the chat room " + s.substring(0, s.indexOf("@")) + ". Invitation comes with content: " + s3);
                        sendBroadcast(openIntent);

                    }
                });
    }

    public void disconnect() {
        if (m_connection != null && !m_connection.isConnected()) {
            m_connection.disconnect();
        }
    }

    public void sendMessage(String text, String to) {
        Message msg = new Message(to, Message.Type.chat);
        msg.setBody(text);
        if (m_connection != null && m_connection.isConnected()) {
            m_connection.sendPacket(msg);
        }
    }

    public XMPPConnection getConnection() {
        return m_connection;
    }

    public Roster getRoster() {
        Roster roster = null;
        if (m_connection != null && m_connection.isConnected()) {
            roster = m_connection.getRoster();
        }

        return roster;
    }


//    public static void configure(ProviderManager pm) {
//
//        // Private Data Storage
//        pm.addIQProvider("query", "jabber:iq:private",
//                new PrivateDataManager.PrivateDataIQProvider());
//
//        // Time
//        try {
//            pm.addIQProvider("query", "jabber:iq:time", Class
//                    .forName("org.jivesoftware.smackx.packet.Time"));
//        } catch (ClassNotFoundException e) {
//            Log.w("TestClient",
//                    "Can't load class for org.jivesoftware.smackx.packet.Time");
//        }
//
//        // Roster Exchange
//        pm.addExtensionProvider("x", "jabber:x:roster",
//                new RosterExchangeProvider());
//
//        // Message Events
//        pm.addExtensionProvider("x", "jabber:x:event",
//                new MessageEventProvider());
//
//        // Chat State
//        pm.addExtensionProvider("active",
//                "http://jabber.org/protocol/chatstates",
//                new ChatStateExtension.Provider());
//
//        pm.addExtensionProvider("composing",
//                "http://jabber.org/protocol/chatstates",
//                new ChatStateExtension.Provider());
//
//        pm.addExtensionProvider("paused",
//                "http://jabber.org/protocol/chatstates",
//                new ChatStateExtension.Provider());
//
//        pm.addExtensionProvider("inactive",
//                "http://jabber.org/protocol/chatstates",
//                new ChatStateExtension.Provider());
//
//        pm.addExtensionProvider("gone",
//                "http://jabber.org/protocol/chatstates",
//                new ChatStateExtension.Provider());
//
//        // XHTML
//        pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im",
//                new XHTMLExtensionProvider());
//
//        // Group Chat Invitations
//        pm.addExtensionProvider("x", "jabber:x:conference",
//                new GroupChatInvitation.Provider());
//
//        // Service Discovery # Items
//        pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
//                new DiscoverItemsProvider());
//
//        // Service Discovery # Info
//        pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
//                new DiscoverInfoProvider());
//
//        // Data Forms
//        pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());
//
//        // MUC User
//        pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
//                new MUCUserProvider());
//
//        // MUC Admin
//        pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
//                new MUCAdminProvider());
//
//        // MUC Owner
//        pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
//                new MUCOwnerProvider());
//
//        // Delayed Delivery
//        pm.addExtensionProvider("x", "jabber:x:delay",
//                new DelayInformationProvider());
//
//        // Version
//        try {
//            pm.addIQProvider("query", "jabber:iq:version", Class
//                    .forName("org.jivesoftware.smackx.packet.Version"));
//        } catch (ClassNotFoundException e) {
//            // Not sure what's happening here.
//        }
//        // VCard
//        pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());
//
//        // Offline Message Requests
//        pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
//                new OfflineMessageRequest.Provider());
//
//        // Offline Message Indicator
//        pm.addExtensionProvider("offline",
//                "http://jabber.org/protocol/offline",
//                new OfflineMessageInfo.Provider());
//
//        // Last Activity
//        pm
//                .addIQProvider("query", "jabber:iq:last",
//                        new LastActivity.Provider());
//
//        // User Search
//        pm
//                .addIQProvider("query", "jabber:iq:search",
//                        new UserSearch.Provider());
//
//        // SharedGroupsInfo
//        pm.addIQProvider("sharedgroup",
//                "http://www.jivesoftware.org/protocol/sharedgroup",
//                new SharedGroupsInfo.Provider());
//
//        // JEP-33: Extended Stanza Addressing
//        pm.addExtensionProvider("addresses",
//                "http://jabber.org/protocol/address",
//                new MultipleAddressesProvider());
//
//        // FileTransfer
//        pm.addIQProvider("si", "http://jabber.org/protocol/si",
//                new StreamInitiationProvider());
//
//        pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
//                new BytestreamsProvider());
//
//        // pm.addIQProvider("open", "http://jabber.org/protocol/ibb",
//        // new IBBProviders.Open());
//        //
//        // pm.addIQProvider("close", "http://jabber.org/protocol/ibb",
//        // new IBBProviders.Close());
//        //
//        // pm.addExtensionProvider("data", "http://jabber.org/protocol/ibb",
//        // new IBBProviders.Data());
//
//        // Privacy
//        pm.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());
//
//        pm.addIQProvider("command", "http://jabber.org/protocol/commands",
//                new AdHocCommandDataProvider());
//        pm.addExtensionProvider("malformed-action",
//                "http://jabber.org/protocol/commands",
//                new AdHocCommandDataProvider.MalformedActionError());
//        pm.addExtensionProvider("bad-locale",
//                "http://jabber.org/protocol/commands",
//                new AdHocCommandDataProvider.BadLocaleError());
//        pm.addExtensionProvider("bad-payload",
//                "http://jabber.org/protocol/commands",
//                new AdHocCommandDataProvider.BadPayloadError());
//        pm.addExtensionProvider("bad-sessionid",
//                "http://jabber.org/protocol/commands",
//                new AdHocCommandDataProvider.BadSessionIDError());
//        pm.addExtensionProvider("session-expired",
//                "http://jabber.org/protocol/commands",
//                new AdHocCommandDataProvider.SessionExpiredError());
//    }


    public void configure(ProviderManager pm) {

        //  Private Data Storage
        pm.addIQProvider("query", "jabber:iq:private", new PrivateDataManager.PrivateDataIQProvider());


        //  Time
        try {
            pm.addIQProvider("query", "jabber:iq:time", Class.forName("org.jivesoftware.smackx.packet.Time"));
        } catch (ClassNotFoundException e) {
            Log.w("TestClient", "Can't load class for org.jivesoftware.smackx.packet.Time");
        }

        //  Roster Exchange
        pm.addExtensionProvider("x", "jabber:x:roster", new RosterExchangeProvider());

        //  Message Events
        pm.addExtensionProvider("x", "jabber:x:event", new MessageEventProvider());

        //  Chat State
        pm.addExtensionProvider("active", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());

        pm.addExtensionProvider("composing", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());

        pm.addExtensionProvider("paused", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());

        pm.addExtensionProvider("inactive", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());

        pm.addExtensionProvider("gone", "http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());

        //  XHTML
        pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im", new XHTMLExtensionProvider());

        //  Group Chat Invitations
        pm.addExtensionProvider("x", "jabber:x:conference", new GroupChatInvitation.Provider());

        //  Service Discovery # Items
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());

        //  Service Discovery # Info
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());

        //  Data Forms
        pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());

        //  MUC User
        pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user", new MUCUserProvider());

        //  MUC Admin
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin", new MUCAdminProvider());


        //  MUC Owner
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner", new MUCOwnerProvider());

        //  Delayed Delivery
        pm.addExtensionProvider("x", "jabber:x:delay", new DelayInformationProvider());

        //  Version
        try {
            pm.addIQProvider("query", "jabber:iq:version", Class.forName("org.jivesoftware.smackx.packet.Version"));
        } catch (ClassNotFoundException e) {
            //  Not sure what's happening here.
        }

        //  VCard
        pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

        //  Offline Message Requests
        pm.addIQProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageRequest.Provider());

        //  Offline Message Indicator
        pm.addExtensionProvider("offline", "http://jabber.org/protocol/offline", new OfflineMessageInfo.Provider());

        //  Last Activity
        pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());

        //  User Search
        pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());

        //  SharedGroupsInfo
        pm.addIQProvider("sharedgroup", "http://www.jivesoftware.org/protocol/sharedgroup", new SharedGroupsInfo.Provider());

        //  JEP-33: Extended Stanza Addressing
        pm.addExtensionProvider("addresses", "http://jabber.org/protocol/address", new MultipleAddressesProvider());

        //   FileTransfer
        pm.addIQProvider("si", "http://jabber.org/protocol/si", new StreamInitiationProvider());

        pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams", new BytestreamsProvider());

//        pm.addIQProvider("open","http://jabber.org/protocol/ibb", new IBBProviders.Open());
//
//        pm.addIQProvider("close","http://jabber.org/protocol/ibb", new IBBProviders.Close());
//
//        pm.addExtensionProvider("data","http://jabber.org/protocol/ibb", new IBBProviders.Data());

        //  Privacy
        pm.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());

        pm.addIQProvider("command", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider());
        pm.addExtensionProvider("malformed-action", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.MalformedActionError());
        pm.addExtensionProvider("bad-locale", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.BadLocaleError());
        pm.addExtensionProvider("bad-payload", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.BadPayloadError());
        pm.addExtensionProvider("bad-sessionid", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.BadSessionIDError());
        pm.addExtensionProvider("session-expired", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.SessionExpiredError());
    }

}